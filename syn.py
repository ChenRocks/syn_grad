import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable

from syngrad.conv import NetA, NetB, I, G


def train(net_a, net_b, net_i, net_g, train_loader,
          optimizer, grad_opt, in_opt):
    net_a.train()
    net_b.train()
    net_i.train()
    net_g.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        one_hot = torch.zeros(target.size(0), 10).scatter_(
            1, target.unsqueeze(1), 1)
        data, one_hot, target = Variable(data), Variable(one_hot), Variable(target)
        optimizer.zero_grad()
        grad_opt.zero_grad()
        in_opt.zero_grad()
        out_a = net_a(data)
        out_a_d = out_a.detach()
        grad_a = net_g(out_a_d, one_hot)
        out_a.backward(grad_a)
        #in_b = net_i(data, one_hot)
        #in_b_d = in_b.detach()
        #in_b_d.requires_grad = True
        #i_loss = F.mse_loss(in_b, out_a_d)
        #i_loss.backward()
        #output = net_b(in_b_d)
        out_a_d.requires_grad=True
        output = net_b(out_a_d)
        loss = F.nll_loss(output, target)
        loss.backward()
        true_grad = out_a_d.grad.detach()
        true_grad.volatile = False
        g_loss = F.mse_loss(grad_a, true_grad)
        g_loss.backward()
        optimizer.step()
        grad_opt.step()
        in_opt.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data[0]))


def test(net_a, net_b, test_loader):
    net_a.eval()
    net_b.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = net_b(net_a(data))
        test_loss += F.nll_loss(output, target, size_average=False).data[0] # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    args = parser.parse_args()
    args.cuda = not args.no_cuda and torch.cuda.is_available()

    torch.set_num_threads(1)  # use only one core
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)

    # build data loaders
    kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.test_batch_size, shuffle=True, **kwargs)


    net_a = NetA()
    net_b = NetB()
    net_i = I()
    net_g = G()
    opt_i = optim.Adam(net_i.parameters())
    opt_g = optim.Adam(net_g.parameters())
    optimizer = optim.Adam([p for n in [net_a, net_b]
                           for p in n.parameters()])
    for epoch in range(1, args.epochs + 1):
        train(net_a, net_b, net_i, net_g, train_loader, optimizer, opt_g, opt_i)
        test(net_a, net_b, test_loader)


