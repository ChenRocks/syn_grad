import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class NetA(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        return x


class NetB(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x)

class I(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv = nn.Conv2d(1+10, 20, kernel_size=5)

    def forward(self, x, y):
        size = y.size() + x.size()[-2:]
        y = y.unsqueeze(2).unsqueeze(3).expand(*size)
        x = F.relu(F.max_pool2d(self.conv(torch.cat([x, y], dim=1)), 6))
        return x

class G(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv = nn.Conv2d(20+10, 20, kernel_size=3, padding=(1, 1))

    def forward(self, x, y):
        size = y.size() + x.size()[-2:]
        y = y.unsqueeze(2).unsqueeze(3).expand(*size)
        grad = self.conv(torch.cat([x, y], dim=1))
        return grad


